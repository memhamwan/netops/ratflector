# Helm chart

This is still a work-in-progress.

## Dependencies

This chart uses bjw-s's [common](https://github.com/bjw-s/helm-charts/tree/main/charts/library/common) chart as a dependency. It offers a lot of flexibility, so be sure to review the `values.yaml` from their repository for where this chart can be leveraged in other use cases and deployment patterns.

## Local testing

```bash
helm install --dry-run --debug mode-bridges .
```
