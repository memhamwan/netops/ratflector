# ratflector

## Local docker execution

As this is a docker image, we assume you already have docker running on your environment. First, build the image via:

```bash
docker build . --tag ratflector
```

Interactive run:

```bash
docker run --name ratflector --rm -i -t ratflector bash
```
