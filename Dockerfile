FROM python:3-bullseye

WORKDIR /usr/src/app

RUN apt-get update && apt-get install --no-install-recommends -y \
  libgirepository1.0-dev=1.66.1-1+b1 \
  libgtk-3-dev=3.24.24-4+deb11u2 \
  && rm -rf /var/lib/apt/lists/*

COPY D-Rats /usr/src/app/

RUN pip install --no-cache-dir -r requirements.txt

ENV GDK_BACKEND=broadway
EXPOSE 9000/tcp
EXPOSE 8080/tcp

ENTRYPOINT [ "python", "d-rats_repeater.py" ]
CMD [ "-C" ]
